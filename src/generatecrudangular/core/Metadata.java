/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generatecrudangular.core;

import generatecrudangular.core.domain.Column;
import generatecrudangular.core.domain.Table;
import generatecrudangular.core.Parse;
import java.io.File;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.HashMap;
import java.util.List;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author viniciusluciene
 */
public class Metadata {

    public static File extract(Connection connect, List<String> allTablesOfDatabase, String tableName, String template, File properties, File outPutFolder, boolean forceEncodingUtf8) throws SQLException, IOException, Exception {
        Table table;
        try {
            DatabaseMetaData dbmd = connect.getMetaData();
            Statement stmt = connect.createStatement();
            ResultSet primaryKeys = dbmd.getPrimaryKeys(null, null, tableName);
            ResultSet foreignKeys = dbmd.getImportedKeys(null, null, tableName);
            ResultSet rset = stmt.executeQuery("SELECT * FROM " + tableName);
            ResultSetMetaData rsmd = rset.getMetaData();
            table = getTable(allTablesOfDatabase, StringUtils.remove(WordUtils.capitalize(tableName, '_'), "_"), rsmd, primaryKeys, foreignKeys);
        } catch (SQLException e) {
            throw new SQLException("Ocorreu um erro ao recuperar o metadata", e);
        }
        HashMap<String, Object> params = new HashMap<String, Object>();
        params.put("table", table);
        if (properties != null) {
            String contentProperties = "";
            try {
                contentProperties = FileUtils.readFileToString(properties);
            } catch (IOException e1) {
                throw new IOException("O arquivo de propriedades nfoi encontrado. ", e1);
            }
            String[] lines = contentProperties.split("[\\r\\n]+");
            for (String line : lines) {
                params.put(line.split("=")[0].trim(), line.split("=")[1].trim());
            }
        }
        return Parse.save(tableName, template, outPutFolder, params, forceEncodingUtf8);
    }

    public static Table getTable(List<String> allTablesOfDatabase, String tableName, ResultSetMetaData rsmd, ResultSet primaryKeys, ResultSet foreignKeys) throws SQLException {
        Table table = new Table(StringUtils.remove(WordUtils.capitalize(tableName, '_'), "_"));
        for (int i = 0; i < rsmd.getColumnCount(); i++) {
            Column column = new Column();
            column.setTable(table);
            column.setName(rsmd.getColumnName(i + 1));
            column.setFisicalName(rsmd.getColumnName(i + 1));
            column.setTypeName(rsmd.getColumnTypeName(i + 1));
            column.setTypeComplete(rsmd.getColumnClassName(i + 1));
            column.setType(StringUtils.substringAfterLast(rsmd.getColumnClassName(i + 1), "."));
            column.setPrecision(rsmd.getPrecision(i + 1));
            column.setLength(rsmd.getColumnDisplaySize(i + 1));
            column.setIdentity(rsmd.isAutoIncrement(i + 1));
            column.setCurrency(rsmd.isCurrency(i + 1));
            column.setNullable(rsmd.isNullable(i + 1));
            table.getColumns().add(column);
        }
        while (primaryKeys.next()) {
            String primaryKeyColumn = primaryKeys.getString("COLUMN_NAME");
            ((Column) table.getColumns().get(table.getColumns().indexOf(new Column(primaryKeyColumn)))).setPrimaryKey(true);
        }
        while (foreignKeys.next()) {
            String foreignKeyColumn = foreignKeys.getString("FKCOLUMN_NAME");
            ((Column) table.getColumns().get(table.getColumns().indexOf(new Column(foreignKeyColumn)))).setForeignKey(true);
            String nomeTabelaPk = foreignKeys.getString("PKTABLE_NAME");
            System.out.println("foreignKeys.getString(\"PKTABLE_NAME\"): " + nomeTabelaPk);
            for (String nomeTabela : allTablesOfDatabase) {
                if (nomeTabela.toLowerCase().equals(nomeTabelaPk.toLowerCase())) {
                    nomeTabelaPk = nomeTabela;
                    break;
                }
            }
            ((Column) table.getColumns().get(table.getColumns().indexOf(new Column(foreignKeyColumn)))).setType(nomeTabelaPk);
            ((Column) table.getColumns().get(table.getColumns().indexOf(new Column(foreignKeyColumn)))).setName(nomeTabelaPk);
        }
        System.out.println("===============================");
        System.out.println("Tabela " + table.getName());
        System.out.println("===============================");
        for (Column c : table.getColumns()) {
            System.out.println(c.getName() + " - " + c.getAtributeName() + " - " + c.getAtributeNameCaseInsensitive() + " - " + c.getType() + " - " + c.getTypeComplete());
        }
        System.out.println("");
        System.out.println("");
        return table;
    }
}
