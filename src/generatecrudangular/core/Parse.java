/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generatecrudangular.core;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;
import java.io.Writer;
import java.text.ParseException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Set;
import jdk.nashorn.internal.runtime.Context;
import jdk.nashorn.internal.runtime.ParserException;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.text.WordUtils;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.exception.ParseErrorException;
import org.apache.velocity.exception.ResourceNotFoundException;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;

/**
 *
 * @author viniciusluciene
 */
public class Parse {

    private static String FILE_SEPARATOR = System.getProperty("file.separator");

    private static String ENCODING = "UTF-8";

    private static String concatFolders(String... folders) {
        String result = "";
        for (String folder : folders) {
            result = result + folder + FILE_SEPARATOR;
        }
        return result;
    }

    /**
     * Salvar os aruqivos gerados pelo banco de dados
     *
     * @param tableName
     * @param template
     * @param projectFolder
     * @param params
     * @param forceEncodingUtf8
     * @return
     * @throws ParserException
     * @throws Exception
     */
    public static File save(String tableName, String template, File projectFolder, HashMap<String, Object> params, boolean forceEncodingUtf8) throws ParserException, Exception {
        Template velTemplate;
        String templateFullPath = FilenameUtils.getFullPath(template);
        String templateName = FilenameUtils.getName(template);
        // Velocity.setProperty("file.resource.loader.path", templateFullPath);
        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());

        try {
            // Velocity.init();
            velocityEngine.init();
        } catch (Exception e) {
            throw new Exception("Ocorreu um erro ao iniciar o Velocity", e);
        }
        try {
            if (forceEncodingUtf8) {
                velTemplate = velocityEngine.getTemplate(template, ENCODING);
            } else {
                velTemplate = velocityEngine.getTemplate(template);
            }
        } catch (ResourceNotFoundException e) {
            throw new ResourceNotFoundException("O template " + templateName + " npser localizado", e);
        } catch (ParseErrorException e) {
            throw new ParseErrorException("Template inv" + e);
        } catch (Exception e) {
            throw new Exception("Erro ao recuperar o template", e);
        }
        VelocityContext ctx = new VelocityContext();
        Writer writer = new StringWriter();
        Set<String> paramsKeys = params.keySet();
        for (String key : paramsKeys) {
            ctx.put(key, params.get(key));
        }
        try {
            velTemplate.merge(ctx, writer);
        } catch (Exception e) {
            throw new Exception("Erro ao aplicar os valores ao template", e);
        }
        String nameOutPut = "";

        if (templateName.equalsIgnoreCase("model.vm")) {
            nameOutPut = StringUtils.remove(WordUtils.capitalize(tableName, '_'), "_")  + "." + FilenameUtils.getExtension(templateName);
        } else if (!templateName.equalsIgnoreCase("model.vm")) {
            nameOutPut = StringUtils.remove(WordUtils.capitalize(tableName, '_'), "_") + templateName;
        }

        String finalPath = "";
        if (nameOutPut.endsWith("vm")) {
            String folderImport = StringUtils.substringBetween(writer.toString(), "package", ";").trim().replace(".", FILE_SEPARATOR);
            finalPath = concatFolders(new String[]{projectFolder.getAbsolutePath(), "src", "main", "java", folderImport});
        }

        (new File(finalPath)).mkdirs();
        File arquivoGerado = new File(finalPath + FILE_SEPARATOR + nameOutPut.replace(".vm", ".java"));
        try {
            if (forceEncodingUtf8) {
                FileUtils.write(arquivoGerado, writer.toString(), ENCODING);
            } else {
                FileUtils.write(arquivoGerado, writer.toString());
            }
        } catch (IOException e) {
            throw new IOException("Erro ao gravar o arquivo jprocessado", e);
        }
        return arquivoGerado;
    }

    /**
     * Salvando arquivos na criação do Projeto
     *
     * @param template
     * @param projectFolder
     * @param params
     * @param forceEncodingUtf8
     * @return
     * @throws ParserException
     * @throws Exception
     */
    public static void save(File projectFolder, String pacote, boolean forceEncodingUtf8) throws ParserException, Exception {
        String pacoteJV = "generatecrudangular" + File.separator + "templates" + File.separator + "jv" + File.separator + "other" + File.separator;

        List<String> templates = Arrays.asList(
                pacoteJV + "config" + File.separator + "AuthorizationServerConfig.vm",
                pacoteJV + "config" + File.separator + "BasicSecurityConfig.vm",
                pacoteJV + "config" + File.separator + "ResourceServerConfig.vm",
                pacoteJV + "config" + File.separator + "SwaggerConfig.vm",
                pacoteJV + "config" + File.separator + "token" + File.separator + "CustomTokenEnhancer.vm",
                pacoteJV + "event" + File.separator + "RecursoCriadoEvent.vm",
                pacoteJV + "event" + File.separator + "listener" + File.separator + "RecursoCriadoListener.vm",
                pacoteJV + "exceptionhandler" + File.separator + "ExceptionHandle.vm",
                pacoteJV + "security" + File.separator + "AppUserDetailsService.vm",
                pacoteJV + "security" + File.separator + "UsuarioSistema.vm",
                pacoteJV + "token" + File.separator + "RefreshTokenCookiePreProcessorFilter.vm",
                pacoteJV + "token" + File.separator + "RefreshTokenPostProcessor.vm"
        );

        for (String template : templates) {
            Template velTemplate;
            String templateFullPath = FilenameUtils.getFullPath(template);
            String templateName = FilenameUtils.getName(template);
            // Velocity.setProperty("file.resource.loader.path", templateFullPath);
            VelocityEngine velocityEngine = new VelocityEngine();
            velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
            velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());

            HashMap<String, Object> params = new HashMap<String, Object>();
            params.put("pkg", pacote);
            params.put("pkgModel", pacote + ".model");
            params.put("pkgRepository", pacote + ".repository");

//        params.put("pkgconfig", pacote + "");
//        params.put("pkgconfigToken", pacote + ".config.token");
//        params.put("pkgEvent", pacote + ".event");
//        params.put("pkgEventListener", pacote + ".event.listener");
//        params.put("pkgExceptionHandler", pacote + ".exceptionhandler");
//        params.put("pkgSecurity", pacote + ".security");
//        params.put("pkgToken", pacote + ".token");
            try {
                // Velocity.init();
                velocityEngine.init();
            } catch (Exception e) {
                throw new Exception("Ocorreu um erro ao iniciar o Velocity", e);
            }
            try {
                if (forceEncodingUtf8) {
                    velTemplate = velocityEngine.getTemplate(template, ENCODING);
                } else {
                    velTemplate = velocityEngine.getTemplate(template);
                }
            } catch (ResourceNotFoundException e) {
                throw new ResourceNotFoundException("O template " + templateName + " npser localizado", e);
            } catch (ParseErrorException e) {
                throw new ParseErrorException("Template inv" + e);
            } catch (Exception e) {
                throw new Exception("Erro ao recuperar o template", e);
            }
            VelocityContext ctx = new VelocityContext();
            Writer writer = new StringWriter();
            Set<String> paramsKeys = params.keySet();
            for (String key : paramsKeys) {
                ctx.put(key, params.get(key));
            }
            try {
                velTemplate.merge(ctx, writer);
            } catch (Exception e) {
                throw new Exception("Erro ao aplicar os valores ao template", e);
            }

            String finalPath = "";

            String folderImport = StringUtils.substringBetween(writer.toString(), "package", ";").trim().replace(".", FILE_SEPARATOR);
            finalPath = concatFolders(new String[]{projectFolder.getAbsolutePath(), "src", "main", "java", folderImport});

            (new File(finalPath)).mkdirs();
            File arquivoGerado = new File(finalPath + FILE_SEPARATOR + templateName.replace(".vm", ".java"));
            try {
                if (forceEncodingUtf8) {
                    FileUtils.write(arquivoGerado, writer.toString(), ENCODING);
                } else {
                    FileUtils.write(arquivoGerado, writer.toString());
                }
            } catch (IOException e) {
                throw new IOException("Erro ao gravar o arquivo jprocessado", e);
            }
        }

        // return arquivoGerado;
    }
}
