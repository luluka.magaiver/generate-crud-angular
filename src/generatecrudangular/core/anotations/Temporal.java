package generatecrudangular.core.anotations;

import generatecrudangular.core.domain.Column;

public class Temporal
        implements ColumnAnnotation {

    private boolean containsTime;

    public Temporal(boolean containsTime) {
        this.containsTime = containsTime;
    }

    public String getAnnotation(Column column) {
        if (this.containsTime) {
            return "@Temporal(TemporalType.TIMESTAMP)";
        }
        return "@Temporal(TemporalType.DATE)";
    }

    public Integer getPriority() {
        return Integer.valueOf(50);
    }
}
