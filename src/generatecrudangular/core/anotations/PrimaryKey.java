package generatecrudangular.core.anotations;

import generatecrudangular.core.domain.Column;

public class PrimaryKey
        implements ColumnAnnotation {

    public String getAnnotation(Column column) {
        if (column.isPrimaryKey()) {
            return "@Id\n";
        }
        return "";
    }

    public Integer getPriority() {
        return Integer.valueOf(10);
    }
}
