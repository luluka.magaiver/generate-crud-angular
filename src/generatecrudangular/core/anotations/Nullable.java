package generatecrudangular.core.anotations;

import generatecrudangular.core.domain.Column;

public class Nullable
        implements ColumnAnnotation {

    public String getAnnotation(Column column) {
        String retorno = "";
        if (!column.isIdentity() && !column.isPrimaryKey() && ((column.isPrimaryKey() && !column.isIdentity()) || !column.isNullable())) {

            retorno = retorno + "@NotNull(message=\"O campo " + column.getName() + " não pode ser vazio.\")";

            if (column.getType().equals("String")) {
                retorno += "\n";
                retorno = retorno + "@Length(min=2, max=" + column.getLength() + ", message=\"O campo " + column.getName() + " deve conter entre {min} e {max} caracteres.\")";
            }
        }
        return retorno;
    }

    public Integer getPriority() {
        return Integer.valueOf(40);
    }
}
