package generatecrudangular.core.anotations;

import generatecrudangular.core.domain.Column;

public class Identity
        implements ColumnAnnotation {

    public String getAnnotation(Column column) {
        if (column.isIdentity()) {
            return "@GeneratedValue(strategy = GenerationType.IDENTITY)\n";
        }
        return "";
    }

    public Integer getPriority() {
        return Integer.valueOf(20);
    }
}
