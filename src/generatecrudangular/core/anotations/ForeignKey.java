package generatecrudangular.core.anotations;

import generatecrudangular.core.domain.Column;

public class ForeignKey
        implements ColumnAnnotation {

    public String getAnnotation(Column column) {
        String retorno = "";
        if (column.isForeignKey()) {
            retorno = retorno + "@ManyToOne(  )";
        }
        return retorno;
    }

    public Integer getPriority() {
        return Integer.valueOf(30);
    }
}
