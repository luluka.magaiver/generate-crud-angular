package generatecrudangular.core.anotations;

import java.util.Comparator;

public class ColumnAnnotationComparator
        implements Comparator {

    public int compare(Object o1, Object o2) {
        ColumnAnnotation a1 = (ColumnAnnotation) o1;
        ColumnAnnotation a2 = (ColumnAnnotation) o2;
        return (a1.getPriority().intValue() < a2.getPriority().intValue()) ? -1 : ((a1.getPriority().intValue() > a2.getPriority().intValue()) ? 1 : 0);
    }
}
