package generatecrudangular.core.anotations;

import generatecrudangular.core.domain.Column;


public interface ColumnAnnotation {
  String getAnnotation(Column paramColumn);
  Integer getPriority();
}