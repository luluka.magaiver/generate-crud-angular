package generatecrudangular.core.anotations;

import generatecrudangular.core.domain.Column;

public class ColumnName
        implements ColumnAnnotation {

    public String getAnnotation(Column column) {
        if (column.isForeignKey()) {
            return "@JoinColumn(name=\"" + column.getFisicalName() + "\")\n";
        }
        return "@Column(name=\"" + column.getFisicalName() + "\", nullable=" + column.isNullable() + ")";
    }

    public Integer getPriority() {
        return Integer.valueOf(50);
    }
}
