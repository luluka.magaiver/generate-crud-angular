package generatecrudangular.core.domain;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author viniciusluciene
 */
public class Table {

    private String name;

    private List<Column> columns = new ArrayList<Column>();

    public Table() {
    }

    public Table(String name) {
        this.name = name;
    }

    public String getName() {
        return WordUtils.capitalize(this.name);
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<Column> getColumns() {
        return this.columns;
    }

    public void setColumns(List<Column> columns) {
        this.columns = columns;
    }

    public String toString() {
        return "Table: " + this.name + ", columns: " + this.columns + "";
    }

    public String getImports() {
        Set<String> listImports = new HashSet<String>();
        for (Column column : this.columns) {
            if (!column.getTypeComplete().startsWith("java.lang")) {
                listImports.add("import " + column.getTypeComplete());
            }
        }
        String result = StringUtils.join(listImports, ";\n").trim();
        if (result.length() > 0) {
            result = result + ";\n";
        }
        return result;
    }

    public String getAtributeName() {
        if (this.name != null && this.name.length() > 1) {
            return this.name.substring(0, 1).toLowerCase() + this.name.substring(1);
        }
        return this.name;
    }
}
