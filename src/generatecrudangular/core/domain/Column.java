package generatecrudangular.core.domain;

import generatecrudangular.core.anotations.ColumnAnnotation;
import generatecrudangular.core.anotations.ColumnAnnotationComparator;
import generatecrudangular.core.anotations.ColumnName;
import generatecrudangular.core.anotations.ForeignKey;
import generatecrudangular.core.anotations.Identity;
import generatecrudangular.core.anotations.Nullable;
import generatecrudangular.core.anotations.PrimaryKey;
import generatecrudangular.core.anotations.Temporal;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import org.apache.commons.lang.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author viniciusluciene
 */
public class Column {

    private Table table;

    private String name;

    private boolean identity;

    private boolean primaryKey;

    private String type;

    private String typeName;

    private String typeComplete;

    private int length;

    private Boolean nullable;

    private int precision;

    private boolean currency;

    private boolean foreignKey;

    private String fisicalName;

    private final List<ColumnAnnotation> columnAnnotation = new ArrayList<ColumnAnnotation>();

    public Column() {
        this.columnAnnotation.add(new ColumnName());
    }

    public Column(String name) {
        this.name = name;
        this.columnAnnotation.add(new ColumnName());
    }

    public Table getTable() {
        return this.table;
    }

    public void setTable(Table table) {
        this.table = table;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean isIdentity() {
        return this.identity;
    }

    public void setIdentity(boolean identity) {
        this.identity = identity;
        this.columnAnnotation.add(new Identity());
    }

    public boolean isPrimaryKey() {
        return this.primaryKey;
    }

    public void setPrimaryKey(boolean primaryKey) {
        this.primaryKey = primaryKey;
        this.columnAnnotation.add(new PrimaryKey());
    }

    public String getType() {
        if (this.type == null || this.type.equals("") || this.type.equals("Clob")) {
            this.type = "String";
        } else if (this.typeName.toLowerCase().equals("nvarchar") && (this.name.toLowerCase().contains("data") || this.name.toLowerCase().contains("date"))) {
            this.type = "Date";
        } else if (this.type.equals("Timestamp")) {
            this.type = "Date";
        }
        return WordUtils.capitalize(this.type);
    }

    public void setType(String type) {
        this.type = type;
        if (this.typeName.toLowerCase().equals("nvarchar") && (this.name.toLowerCase().contains("data") || this.name.toLowerCase().contains("date"))) {
            this.columnAnnotation.add(new Temporal(false));
        } else if (type.equals("Timestamp")) {
            this.columnAnnotation.add(new Temporal(true));
        }
    }

    public String getTypeName() {
        return this.typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    public String getTypeComplete() {
        if (this.typeComplete == null || this.typeComplete.equals("") || this.typeComplete.toLowerCase().endsWith("clob")) {
            this.typeComplete = "java.lang.String";
        } else if (this.typeName.toLowerCase().endsWith("nvarchar") && (this.name.toLowerCase().contains("data") || this.name.toLowerCase().contains("date"))) {
            this.typeComplete = "java.util.Date";
        } else if (this.typeComplete.toLowerCase().endsWith("timestamp")) {
            this.typeComplete = "java.util.Date";
        }
        return this.typeComplete;
    }

    public void setTypeComplete(String typeComplete) {
        this.typeComplete = typeComplete;
    }

    public int getLength() {
        return this.length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public Boolean getNullable() {
        return this.nullable;
    }

    public void setNullable(Boolean nullable) {
        this.nullable = nullable;
        this.columnAnnotation.add(new Nullable());
    }

    public void setNullable(int nullable) {
        setNullable(Boolean.valueOf((nullable == 1)));
    }

    public int getPrecision() {
        return this.precision;
    }

    public void setPrecision(int precision) {
        this.precision = precision;
    }

    public boolean isCurrency() {
        return this.currency;
    }

    public void setCurrency(boolean currency) {
        this.currency = currency;
    }

    public boolean isForeignKey() {
        return this.foreignKey;
    }

    public void setForeignKey(boolean foreignKey) {
        this.foreignKey = foreignKey;
        this.columnAnnotation.add(new ForeignKey());
    }

    public String getFisicalName() {
        return this.fisicalName;
    }

    public void setFisicalName(String fisicalName) {
        this.fisicalName = fisicalName;
    }

    public boolean isNullable() {
        return (this.nullable == null || this.nullable.booleanValue() == true);
    }

    public String getNameToLower() {
        return firstCharLower(getName());
    }

    public String toString() {
        return "Column [name=" + this.name + ", identity=" + this.identity + ", primaryKey=" + this.primaryKey + ", type=" + this.type + ", length=" + this.length + ", nullable=" + this.nullable + ", precision=" + this.precision + ", currency=" + this.currency + "]";
    }

    public int hashCode() {
        int prime = 31;
        int result = 1;
        result = 31 * result + ((this.name == null) ? 0 : this.name.hashCode());
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Column other = (Column) obj;
        if (this.name == null) {
            if (other.name != null) {
                return false;
            }
        } else if (!this.name.equals(other.name)) {
            return false;
        }
        return true;
    }

    public String firstCharUpper(String value) {
        if (value != null && value.length() > 1) {
            return value.substring(0, 1).toUpperCase() + value.substring(1);
        }
        return value;
    }

    public String firstCharLower(String value) {
        if (value != null && value.length() > 1) {
            return value.substring(0, 1).toLowerCase() + value.substring(1);
        }
        return value;
    }

    public String getAtributeName() {
        if (this.name != null && this.name.length() > 1) {
            if (isPrimaryKey()) {
                return "id";
            }
            return StringUtils.remove(WordUtils.capitalize(this.name, '_'), "_"); // this.name.substring(0, 1).toLowerCase() + this.name.substring(1);
        }
        return this.name;
    }
    
    public String getAtributeNameCaseInsensitive() {
        if (this.name != null && this.name.length() > 1) {
            if (isPrimaryKey()) {
                return "id";
            }
            String upper = StringUtils.remove(WordUtils.capitalize(this.name, '_'), "_");
            String nomeCamelCase = Character.toLowerCase(upper.charAt(0)) + upper.substring(1);
            
            return nomeCamelCase;
        }
        return this.name;
    }

    public String getAnnotations() {
        String retorno = "";
        Collections.sort(this.columnAnnotation, (Comparator<? super ColumnAnnotation>) new ColumnAnnotationComparator());
        for (ColumnAnnotation annot : this.columnAnnotation) {
            retorno = retorno + annot.getAnnotation(this);
        }
        return retorno;
    }

    public String getGetter() {
        String retorno = "";
        if(isForeignKey()){
            retorno = retorno + "public " + firstCharUpper(this.type) + " get" + firstCharUpper(getAtributeName()) + "() {\n";
        } else {
            retorno = retorno + "public " + this.type + " get" + firstCharUpper(getAtributeName()) + "() {\n";
        }
        
        retorno = retorno + "  return " + getAtributeNameCaseInsensitive() + ";\n";
        retorno = retorno + "} \n";
        
        return retorno;
    }

    public String getSetter() {
        String retorno = "";
        if(isForeignKey()){
            retorno = retorno + "public void set" + firstCharUpper(getAtributeName()) + "(" + firstCharUpper(this.type) + " " + getAtributeNameCaseInsensitive() + ") {\n";
        } else {
            retorno = retorno + "public void set" + firstCharUpper(getAtributeName()) + "(" + this.type + " " + getAtributeNameCaseInsensitive() + ") {\n";
        }
        
        retorno = retorno + "  this." + getAtributeNameCaseInsensitive() + " = " + getAtributeNameCaseInsensitive() + ";\n";
        retorno = retorno + "} \n";
        
        return retorno;
    }

    public String getFieldName() {
        return this.table.getAtributeName() + "." + getAtributeName();
    }
}
