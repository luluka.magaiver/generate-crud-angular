/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generatecrudangular.utils;

import generatecrudangular.view.frmProjetoSTS;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.PrintWriter;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 *
 * @author viniciusluciene
 */
public class UnzipUtils {

    private static final int BUFFER = 4096;
    private static String URL_SPRINGTOOLSSUITE = "https://start.spring.io/starter.zip?name=Teste&groupId=br.com.mvsistema&artifactId=Teste&version=1.0.0&description=&packageName=br.com.mvsistema.teste&type=maven-project&packaging=jar&javaVersion=1.8&language=java&bootVersion=2.3.1.RELEASE&dependencies=devtools&dependencies=flyway&dependencies=mysql&dependencies=data-jpa&dependencies=security&dependencies=web";

    /**
     * Extrair arquivo zip
     *
     * @param zipFilePath ex:
     * Paths.get("<CAMINHO DO ARQUIVO ZIP BAIXADO>").getFileName().toString()
     * @param destDirectory ex:
     * Paths.get("<CAMINHO DO DIRETORIO PARA DESCOMPACTAR>").getFileName().toString()
     * @throws IOException
     */
    public static void unzip(String zipFilePath, String destDirectory) {
        File destDir = new File(destDirectory);
        if (!destDir.exists()) {
            destDir.mkdir();
        }
        try (ZipInputStream zipIn = new ZipInputStream(new FileInputStream(zipFilePath))) {
            ZipEntry entry = zipIn.getNextEntry();
            while (entry != null) {
                String filePath = destDirectory + File.separator + entry.getName();
                if (!entry.isDirectory()) {
                    extractFile(zipIn, filePath);
                    System.out.println(filePath);
                } else {
                    File dir = new File(filePath);
                    System.out.println(filePath);
                    dir.mkdir();
                }
                zipIn.closeEntry();
                entry = zipIn.getNextEntry();
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UnzipUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UnzipUtils.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Metodo para fazer o download do arquivo
     *
     * @param destino
     * @throws Exception
     */
    public static final void download(String origem, String destino) {
        gravaArquivoDeURL(origem, destino);
    }

    /**
     * Extracts a zip entry (file entry)
     *
     * @param zipIn
     * @param filePath
     * @throws IOException
     */
    private static void extractFile(ZipInputStream zipIn, String filePath) {
        try (BufferedOutputStream bos = new BufferedOutputStream(new FileOutputStream(filePath))) {
            byte[] bytesIn = new byte[BUFFER];
            int read = 0;
            while ((read = zipIn.read(bytesIn)) != -1) {
                bos.write(bytesIn, 0, read);
            }
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UnzipUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UnzipUtils.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    /**
     * Gravar arquivo de uma url especifica
     *
     * @param stringUrl
     * @param pathLocal
     * @throws Exception
     */
    private static final void gravaArquivoDeURL(String stringUrl, String pathLocal) {

        FileOutputStream os = null;
        try {
            URL url = new URL(stringUrl);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("GET");
            InputStream in = (InputStream) connection.getInputStream();
            String type = connection.getContentType();
            String zip = "application/zip";
            String doc = "application/msword";
            String docx = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
            if (type.equalsIgnoreCase(zip)) {
                pathLocal += ".zip";
            } else if (type.equalsIgnoreCase(doc)) {
                pathLocal += ".doc";

            } else if (type.equalsIgnoreCase(docx)) {
                pathLocal += ".docx";
            }
            File file = new File(pathLocal);
            os = new FileOutputStream(file);
            byte[] buf = new byte[1024];
            int len;
            while ((len = in.read(buf)) > 0) {
                for (int i = 0; i < len; i++) {
                    os.write(buf[i]);
                }
            }
            os.flush();
            os.close();
            connection.disconnect();
            // close the stream
            in.close();
        } catch (FileNotFoundException ex) {
            Logger.getLogger(UnzipUtils.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(UnzipUtils.class.getName()).log(Level.SEVERE, null, ex);
        } finally {
            try {
                os.close();
            } catch (IOException ex) {
                Logger.getLogger(UnzipUtils.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }

    public static void escreverArquivoConfig(File arquivo, String pacote) {

        try {
            arquivo.createNewFile();

            FileWriter arq = new FileWriter(arquivo);

            PrintWriter gravarArq = new PrintWriter(arq);
            gravarArq.printf("pkg=" + pacote + "\n");
            gravarArq.printf("pkgModel=" + pacote + ".model\n");
            gravarArq.printf("pkgFilter=" + pacote + ".filter\n");
            gravarArq.printf("pkgRepository=" + pacote + ".repository\n");
            gravarArq.printf("pkgRepositoryHelper=" + pacote + ".repository.helper\n");
            gravarArq.printf("pkgRepositoryImpl=" + pacote + ".repository.impl\n");
            gravarArq.printf("pkgResource=" + pacote + ".resource\n");
            gravarArq.printf("pkgService=" + pacote + ".service");

            arq.close();
        } catch (IOException ex) {
            Logger.getLogger(frmProjetoSTS.class.getName()).log(Level.SEVERE, null, ex);
        }

    }
    
    public static void escreverArquivosDoProjeto(File destino){
        try {
            
        } catch (Exception e) {
        }
    }
}
