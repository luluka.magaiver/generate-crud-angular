/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generatecrudangular.model;

/**
 *
 * @author viniciusluciene
 */
public class ItemTabela {
    
    private String nomeColuna;
    private String nomeColunaClass;
    private String tipo;
    private String tipoConvertido;

    public ItemTabela() {
    }

    public ItemTabela(String nomeColuna, String nomeColunaClass, String tipo, String tipoConvertido) {
        this.nomeColuna = nomeColuna;
        this.nomeColunaClass = nomeColunaClass;
        this.tipo = tipo;
        this.tipoConvertido = tipoConvertido;
    }

    /**
     * @return the nomeColuna
     */
    public String getNomeColuna() {
        return nomeColuna;
    }

    /**
     * @param nomeColuna the nomeColuna to set
     */
    public void setNomeColuna(String nomeColuna) {
        this.nomeColuna = nomeColuna;
    }

    /**
     * @return the tipo
     */
    public String getTipo() {
        return tipo;
    }

    /**
     * @param tipo the tipo to set
     */
    public void setTipo(String tipo) {
        this.tipo = tipo;
    }

    /**
     * @return the nomeColunaClass
     */
    public String getNomeColunaClass() {
        return nomeColunaClass;
    }

    /**
     * @param nomeColunaClass the nomeColunaClass to set
     */
    public void setNomeColunaClass(String nomeColunaClass) {
        this.nomeColunaClass = nomeColunaClass;
    }

    /**
     * @return the tipoConvertido
     */
    public String getTipoConvertido() {
        return tipoConvertido;
    }

    /**
     * @param tipoConvertido the tipoConvertido to set
     */
    public void setTipoConvertido(String tipoConvertido) {
        this.tipoConvertido = tipoConvertido;
    }
}
