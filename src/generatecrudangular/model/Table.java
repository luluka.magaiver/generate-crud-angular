/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generatecrudangular.model;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author viniciusluciene
 */
public class Table {

    private String nomeTabela;
    private List<ItemTabela> campos = new ArrayList<>();

    public Table() {
    }

    public Table(String nomeTabela, List<ItemTabela>  campos) {
        this.nomeTabela = nomeTabela;
        this.campos = campos;
    }

    /**
     * @return the nomeTabela
     */
    public String getNomeTabela() {
        return nomeTabela;
    }

    /**
     * @param nomeTabela the nomeTabela to set
     */
    public void setNomeTabela(String nomeTabela) {
        this.nomeTabela = nomeTabela;
    }

    /**
     * @return the campos
     */
    public List<ItemTabela>  getCampos() {
        return campos;
    }

    /**
     * @param campos the campos to set
     */
    public void setCampos(List<ItemTabela>  campos) {
        this.campos = campos;
    }

}
