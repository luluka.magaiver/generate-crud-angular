package $pkgRepositoryImpl;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.TypedQuery;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.hibernate.criterion.MatchMode;
import org.hibernate.criterion.Restrictions;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.util.StringUtils;

import ${pkgRepositoryHelper}.${table.name}Helper;
import ${pkgFilter}.${table.name}Filter;
import ${pkgModel}.${table.name};


public class ${table.name}RepositoryImpl implements ${table.name}Helper {

	@PersistenceContext
	private EntityManager manager;
	
	@Override
	public Page<${table.name}> filtrar(${table.name}Filter  ${table.name.toLowerCase()}Filter, Pageable pageable) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<${table.name}> criteria = builder.createQuery(${table.name}.class);
		Root<${table.name}> root = criteria.from(${table.name}.class);
		
		Predicate[] predicates = criarRestrincoes(${table.name.toLowerCase()}Filter, builder, root);
		criteria.where(predicates);
		
		TypedQuery<${table.name}> query = manager.createQuery(criteria);
		adicionarRestricoesDePaginacao(query, pageable);
		
		return new PageImpl<>(query.getResultList(), pageable, total(${table.name.toLowerCase()}Filter));
	}
	
	
	private Predicate[] criarRestrincoes(${table.name}Filter  ${table.name.toLowerCase()}Filter, CriteriaBuilder builder, Root<${table.name}> root) {
		List<Predicate> predicates = new ArrayList<Predicate>();

		if (${table.name.toLowerCase()}Filter != null) {
      #foreach( $column in $table.columns )
          #if($!column.foreignKey)
          ##se for ForeignKey -> Montar Combo
              if(${table.name.toLowerCase()}Filter.get${column.atributeName}() != null) {
                  predicates.add(builder.equal(root.get("${column.atributeNameCaseInsensitive}"), ${table.name.toLowerCase()}Filter.get${column.atributeName}()));
              }
          #elseif( ($!column.type == 'Date' || $!column.type == 'Timestamp') && !$!column.primaryKey)
          ##se for campo Data
              if(${table.name.toLowerCase()}Filter.get${column.atributeName}() != null) {
                  predicates.add(builder.equal(root.get("${column.atributeNameCaseInsensitive}"), ${table.name.toLowerCase()}Filter.get${column.atributeName}()));
              }
          #elseif($!column.type == 'Boolean' && !$!column.primaryKey)
          ##se for campo Boolean -> CheckBox
              // 
          #elseif($!column.type == 'BigDecimal' && !$!column.primaryKey)
          ##se for campo BigDecimal

          #elseif($!column.type == 'Integer' && !$!column.primaryKey)
          ##se for campo Integer
              if(${table.name.toLowerCase()}Filter.get${column.atributeName}() != null) {
                  predicates.add(builder.equal(root.get("${column.atributeNameCaseInsensitive}"), ${table.name.toLowerCase()}Filter.get${column.atributeName}()));
              }
          #elseif($!column.type == 'String' && !$!column.primaryKey)                    
          ##se for campo String
              if (!StringUtils.isEmpty(${table.name.toLowerCase()}Filter.get${column.atributeName}())) {				
                  predicates.add(builder.like(builder.lower(root.get("${column.atributeNameCaseInsensitive}")),"%" + ${table.name.toLowerCase()}Filter.get${column.atributeName}().toLowerCase() + "%"));
              }
          #end ##end -> if($!column.foreignKey)
      #end ##end -> foreach   
		}
		
		return predicates.toArray(new Predicate[predicates.size()]);
	}
	
	private void adicionarRestricoesDePaginacao(TypedQuery<?> query, Pageable pageable) {
		int paginaAtual = pageable.getPageNumber();
		int totalRegistrosPorPagina = pageable.getPageSize();
		int primeiroRegistroDaPagina = paginaAtual * totalRegistrosPorPagina;
		
		query.setFirstResult(primeiroRegistroDaPagina);
		query.setMaxResults(totalRegistrosPorPagina);
	}
	
	private Long total(${table.name}Filter  ${table.name.toLowerCase()}Filter) {
		CriteriaBuilder builder = manager.getCriteriaBuilder();
		CriteriaQuery<Long> criteria = builder.createQuery(Long.class);
		Root<${table.name}> root = criteria.from(${table.name}.class);
		
		Predicate[] predicates = criarRestrincoes(${table.name.toLowerCase()}Filter, builder, root);
		criteria.where(predicates);
		
		criteria.select(builder.count(root));
		return manager.createQuery(criteria).getSingleResult();
	}

}