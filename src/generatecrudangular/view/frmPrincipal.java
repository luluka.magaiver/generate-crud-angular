/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package generatecrudangular.view;

import generatecrudangular.model.Conexao;
import generatecrudangular.model.ItemTabela;
import generatecrudangular.model.Table;
import java.awt.event.ItemEvent;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.StringWriter;
import java.net.URISyntaxException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.sql.Types;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.stream.Collectors;
import java.util.stream.Stream;
// import javafx.scene.control.TreeItem;
import javax.swing.DefaultListModel;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.ListSelectionModel;
import javax.swing.SwingUtilities;
import javax.swing.UIManager;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import org.apache.velocity.Template;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.VelocityEngine;
import org.apache.velocity.runtime.RuntimeConstants;
import org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader;
//import org.apache.velocity.util.StringUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.commons.lang3.text.WordUtils;

/**
 *
 * @author viniciusluciene
 */
public class frmPrincipal extends javax.swing.JFrame {

    private Conexao conexao;
    private Table tabela;
    private Table tabelaSelecionada;
    private final List<Table> tabelas;
    private final List<Table> tabelasSelecionadas;
    File modelHtml = null;
    File modelTs = null;
    File modelCss = null;
    File modelHtmlDet = null;
    File modelTsDet = null;
    File modelCssDet = null;
    File modelService = null;
    File modelClass = null;
    File modelFilterClass = null;
    File dadosAdd = null;

    /**
     * Creates new form frmPrincipal
     */
    public frmPrincipal() {
        initComponents();
        this.setLocationRelativeTo(null);
        jList1.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
        jBGerar.setEnabled(false);
        conexao = new Conexao();
        tabelas = new ArrayList<>();
        tabelasSelecionadas = new ArrayList<>();
        ItemSelecionadoComboBox(jCB_Sgdb.getSelectedItem().toString());
        jCB_Sgdb.addItemListener((ItemEvent e) -> {
            //
            if (e.getStateChange() == ItemEvent.SELECTED) {
                //pegando o texto do item selecionado
                String valorSelecionado = e.getItem().toString();
                ItemSelecionadoComboBox(valorSelecionado);
            }
        });

        jList1.addListSelectionListener((ListSelectionEvent e) -> {
            DefaultListModel model = new DefaultListModel();
            if (!e.getValueIsAdjusting()) {
                model.clear();
                for (int i = 0; i < tabelas.size(); i++) {
                    if (tabelas.get(i).getNomeTabela().equals(jList1.getSelectedValue())) {
                        tabelaSelecionada = tabelas.get(i);
                        for (ItemTabela campo : tabelaSelecionada.getCampos()) {
                            model.addElement(campo.getNomeColunaClass() + " - " + campo.getTipo() + " - " + campo.getNomeColuna());
                            jList2.setModel(model);
                        }
                    }
                }
            }
        });
    }

    private void ItemSelecionadoComboBox(String item) {
        limparCampos();
        switch (item) {
            case "Postgress":
                conexao = new Conexao("jdbc:postgresql://#SERVIDOR:5432/#BANCO", "", "", "org.postgresql.Driver", "", "", "5432");
                jTServidor.setText("localhost");
                jTUsuario.setText("postges");
                jPassSenha.setText("");
                break;

            case "MySql":
                conexao = new Conexao("jdbc:mysql://#SERVIDOR:3306/#BANCO", "", "", "com.mysql.jdbc.Driver", "", "", "3306");
                jTServidor.setText("localhost");
                jTUsuario.setText("root");
                jPassSenha.setText("root");
                break;

            case "Sql Serve":
                conexao = new Conexao("jdbc:sqlserver://#SERVIDOR:1433;databaseName=#BANCO", "", "", "net.sourceforge.jtds.jdbc.Driver", "", "", "1433");
                jTServidor.setText("localhost");
                jTUsuario.setText("sa");
                jPassSenha.setText("");
                break;

            default:
                System.out.printf("Você digitou uma operação inválida.\n");

        }
    }

    private Connection conectaBanco(Conexao conexao) throws Exception {
        Class.forName(conexao.getDriver());
        return DriverManager.getConnection(conexao.getUrl(), conexao.getUsuario(), conexao.getSenha());
    }

    private void criarListTabela(Connection conn) {
        try {
            DefaultListModel model = new DefaultListModel();
            DatabaseMetaData databaseMetaData = conn.getMetaData();

            ResultSet rsTabelas = databaseMetaData.getTables(null, null, "%", new String[]{"TABLE"});
            ResultSet rsCampos;

            String nomeTabela;
            String nomeArquivo;

            while (rsTabelas.next()) {
                int count = 1;
                
                nomeTabela = rsTabelas.getString("TABLE_NAME");
            
                ResultSet rsKeys = databaseMetaData.getImportedKeys(conn.getCatalog(), null, nomeTabela);
                
                while(rsKeys.next()) {
                    String fkTableName = rsKeys.getString("FKTABLE_NAME");
                    String fkColumnName = rsKeys.getString("FKCOLUMN_NAME");
                    int fkSequence = rsKeys.getInt("KEY_SEQ");
                }
                
                tabela = new Table();
                tabela.setNomeTabela(nomeTabela);

                rsCampos = databaseMetaData.getColumns(null, null, nomeTabela, null);
                ResultSetMetaData rsmd = rsCampos.getMetaData();
//                int numberOfColumns = rsmd.getColumnCount();

                while (rsCampos.next()) {
                    String nomeColuna = rsCampos.getString("COLUMN_NAME");
                    int dataType = rsCampos.getInt("DATA_TYPE");

                    ItemTabela itemTabela = new ItemTabela(converterUpperStringCamel(nomeColuna), nomeColuna, tipoCampo(dataType), tipoTabelaConvertido(dataType));
                    tabela.getCampos().add(itemTabela);
                    count++;
                }

                tabelas.add(tabela);

                model.addElement(tabela.getNomeTabela());
                jList1.setModel(model);
            }
        } catch (SQLException ex) {
            Logger.getLogger(frmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void limparCampos() {
        conexao.setBanco("");
        conexao.setDriver("");
        conexao.setPorta("");
        conexao.setSenha("");
        conexao.setUrl("");
        conexao.setUsuario("");
    }

    private String converterUpperStringCamel(String texto) {
        String upper = StringUtils.remove(WordUtils.capitalize(texto, '_'), "_");
        String nomeCamelCase = Character.toLowerCase(upper.charAt(0)) + upper.substring(1);
        return nomeCamelCase;
    }

    private String tipoTabelaConvertido(int dataType) {
        switch (dataType) {
            case Types.INTEGER:
            case Types.DOUBLE:
            case Types.FLOAT:
            case Types.SMALLINT:
            case Types.REAL:
            case Types.NUMERIC:
            case Types.DECIMAL:
            case Types.BIGINT:
                return "number";
            case Types.VARCHAR:
            case Types.NVARCHAR:
            case Types.NCHAR:
            case Types.LONGVARCHAR:
            case Types.LONGNVARCHAR:
                return "string";
            case Types.BOOLEAN:
            case Types.BIT:
                return "boolean";
            case Types.TIME:
            case Types.TIMESTAMP:
            case Types.DATE:
                return "Date";
            default:
                return "";
        }
    }

    /**
     * Metodo que distingue o tipo da variavle do banco de Dados
     *
     * @param tipo
     * @return
     */
    private String tipoCampo(int tipo) {
        switch (tipo) {
            case 1:
                return "CHAR";
            case 2:
                return "NUMERIC";
            case 3:
                return "DECIMAL";
            case 4:
                return "INTEGER";
            case 5:
                return "SMALLINT";
            case 6:
                return "FLOAT";
            case 7:
                return "REAL";
            case 8:
                return "DOUBLE";
            case 0:
                return "NULL";
            case -7:
                return "BIT";
            case -6:
                return "TINYINT";
            case -5:
                return "BIGINT";
            case 12:
                return "VARCHAR";
            case -1:
                return "LONGVARCHAR";
            case 91:
                return "DATE";
            case 92:
                return "TIME";
            case 93:
                return "TIMESTAMP";
            case -2:
                return "BINARY";
            case -3:
                return "VARBINARY";
            case -4:
                return "LONGVARBINARY";
            case 1111:
                return "OTHER";
            case 2000:
                return "JAVA_OBJECT";
            case 2001:
                return "DISTINCT";
            case 2002:
                return "STRUCT";
            case 2003:
                return "ARRAY";
            case 2004:
                return "BLOB";
            case 2005:
                return "CLOB";
            case 2006:
                return "REF";
            case 70:
                return "DATALINK";
            case 16:
                return "BOOLEAN";
            case -8:
                return "ROWID";
            case -15:
                return "NCHAR";
            case -9:
                return "NVARCHAR";
            case -16:
                return "LONGNVARCHAR";
            case 2011:
                return "NCLOB";
            case 2009:
                return "SQLXML";
            case 2012:
                return "REF_CURSOR";
            case 2013:
                return "TIME_WITH_TIMEZONE";
            case 2014:
                return "TIMESTAMP_WITH_TIMEZONE";

        }
        return "";
    }

    /**
     * Metodo responsavel que gera a pasta com os arquivos responsaveis:
     * *.component.ts, *.component.html e *.component.css
     *
     * @param tabela
     * @throws URISyntaxException
     * @throws IOException
     */
    private void gerarTemplates(Table tabela) throws URISyntaxException, IOException {

        criarDiretorios("-cadastro");
        criarDiretorios("-pesquisa");
        
//        criarModelo();
//        
//        criarService();

        VelocityEngine velocityEngine = new VelocityEngine();
        velocityEngine.setProperty(RuntimeConstants.RESOURCE_LOADER, "classpath");
        velocityEngine.setProperty("classpath.resource.loader.class", ClasspathResourceLoader.class.getName());
        velocityEngine.init();

        // InputStream fi = getClass().getClassLoader().getResourceAsStream("generatecrudangular/templates/modelTs.vm");
        // File modelTs = new File(getClass().getClassLoader().getResource("generatecrudangular/templates/modelTs.vm").toURI());
        // File modelHtml = new File(getClass().getClassLoader().getResource("generatecrudangular/templates/modelHtml.vm").toURI());
        Template modelTsTemplate = velocityEngine.getTemplate("generatecrudangular/templates/modelTs.vm"); // modelTs.toString()
        Template modelHtmlTemplate = velocityEngine.getTemplate("generatecrudangular/templates/modelHtml.vm");// modelHtml.toString()

        Template modelTsDetTemplate = velocityEngine.getTemplate("generatecrudangular/templates/modelDetalheTs.vm"); // modelTs.toString()
        Template modelHtmlDetTemplate = velocityEngine.getTemplate("generatecrudangular/templates/modelDetalheHtml.vm");// modelHtml.toString()

        Template modelDadosAdd = velocityEngine.getTemplate("generatecrudangular/templates/dadosAdicionais.vm");
        
        Template modelo = velocityEngine.getTemplate("generatecrudangular/templates/model.vm");
        Template modeloFilter = velocityEngine.getTemplate("generatecrudangular/templates/modelFilter.vm");
        
        Template servico = velocityEngine.getTemplate("generatecrudangular/templates/service.vm");

        VelocityContext context = new VelocityContext();
        context.put("table", tabela);
        context.put("tableName", tabela.getNomeTabela());
        context.put("columns", tabela.getCampos());
        context.put("size", tabela.getCampos().size());
        context.put("className", converterUpperStringCamel(tabela.getNomeTabela()));
        context.put("classNameUP", tabela.getNomeTabela().toUpperCase());
        context.put("classNameUpper", StringUtils.capitalize(converterUpperStringCamel(tabela.getNomeTabela()))
                // StringUtils.capitalize(tabela.getNomeTabela())
        );// FirstLetter

        createModelAndModelDetalhe(modelTsTemplate, modelHtmlTemplate, context, modelTs, modelHtml);
        createModelAndModelDetalhe(modelTsDetTemplate, modelHtmlDetTemplate, context, modelTsDet, modelHtmlDet);
        createTemplateAndFile(modelDadosAdd, context, dadosAdd);// criando arquivo txrt com algumas configurações
        createTemplateAndFile(modelo, context, modelClass);// criando a classe de Modelo
        createTemplateAndFile(modeloFilter, context, modelFilterClass);// criando a classe de ModeloFilter
        createTemplateAndFile(servico, context, modelService);// criando a classe de Serviço
    }

    private void criarDiretorios(String detalhe) throws IOException {
        File diretorioPai = new File(jTCaminhoViews.getText().concat(File.separator + tabelaSelecionada.getNomeTabela().concat("")));
        
        if (!diretorioPai.exists()) {
            diretorioPai.mkdir();
            
        }
        
        File diretorio = new File(diretorioPai.getAbsolutePath().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(detalhe)));

        if (!diretorio.exists()) {
            diretorio.mkdir();
        }

        if (detalhe.equals("-cadastro")) {
            modelHtmlDet = new File(diretorio.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(detalhe).concat(".component.html")));
            modelTsDet = new File(diretorio.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(detalhe).concat(".component.ts")));
            modelCssDet = new File(diretorio.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(detalhe).concat(".component.css")));
            
            dadosAdd = new File(diretorioPai.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(detalhe).concat(".txt")));
            modelService = new File(diretorioPai.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(".service.ts")));
            modelClass = new File(diretorioPai.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(".ts")));
            modelFilterClass = new File(diretorioPai.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat("-filter.ts")));
            
            modelCssDet.createNewFile();
            modelHtmlDet.createNewFile();
            modelTsDet.createNewFile();
            
            dadosAdd.createNewFile();
            modelService.createNewFile();
            modelClass.createNewFile();
            modelFilterClass.createNewFile();
        } else {
            modelHtml = new File(diretorio.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(detalhe).concat(".component.html")));
            modelTs = new File(diretorio.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(detalhe).concat(".component.ts")));
            modelCss = new File(diretorio.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(detalhe).concat(".component.css")));
            
            modelCss.createNewFile();
            modelHtml.createNewFile();
            modelTs.createNewFile();
        }

    }

    private void criarService() {
        File diretorio = new File(jTCaminhoViews.getText().concat("service"));

        if (!diretorio.exists()) {
            diretorio.mkdir();
        }
        modelService = new File(diretorio.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(".service.ts")));
    }

    private void criarModelo() {
        File diretorio = new File(jTCaminhoViews.getText().concat("model"));

        if (!diretorio.exists()) {
            diretorio.mkdir();
        }
        modelClass = new File(diretorio.toString().concat(File.separator + tabelaSelecionada.getNomeTabela().concat(".ts")));
    }

    /**
     * Metodo para pegar a lista de templates do Modelo
     *
     * @param velocityEngine
     * @return
     */
    private List<Template> templatesModel(VelocityEngine velocityEngine) {
        List<Template> templates = new ArrayList<>();
        templates.add(velocityEngine.getTemplate("generatecrudangular/templates/modelTs.vm"));
        templates.add(velocityEngine.getTemplate("generatecrudangular/templates/modelHtml.vm"));
        return templates;
    }

    /**
     * Metodo para pegar a lista de Templates do detalhe do Modelo
     *
     * @param velocityEngine
     * @return
     */
    private List<Template> templateModelDetalhe(VelocityEngine velocityEngine) {
        List<Template> templates = new ArrayList<>();
        templates.add(velocityEngine.getTemplate("generatecrudangular/templates/modelDetalheTs.vm"));
        templates.add(velocityEngine.getTemplate("generatecrudangular/templates/modelDetalheHtml.vm"));
        return templates;
    }
    
    private Table tabelaPeloNome(String nomeTabela){
        for(Table t : tabelas){
            if(t.getNomeTabela().equalsIgnoreCase(nomeTabela)){
                return t;
            }
        }
        return null;
    }

    /**
     * Metodo que escreve o arquivo a partir dos dados da tabela e do modelo do
     * template
     *
     * @param template
     * @param context
     * @param fileModel
     * @throws IOException
     */
    private void createModelAndModelDetalhe(Template templateTs, Template templateHtml, VelocityContext context, File modelTs, File modelHtml) throws IOException {
        StringWriter modelTsTempSb = new StringWriter();
        templateTs.merge(context, modelTsTempSb);
        System.out.println(modelTsTempSb.toString());

        try (FileWriter arq = new FileWriter(modelTs.toString()); BufferedWriter writer = new BufferedWriter(arq)) {
            writer.write(modelTsTempSb.toString());
            writer.flush();
        }

        StringWriter modelHtmlSb = new StringWriter();
        templateHtml.merge(context, modelHtmlSb);
        System.out.println(modelHtmlSb.toString());

        try (FileWriter arq1 = new FileWriter(modelHtml.toString()); BufferedWriter writer = new BufferedWriter(arq1)) {
            writer.write(modelHtmlSb.toString());
            writer.flush();
        }
    }

    private void createTemplateAndFile(Template templateDadosAdd, VelocityContext context, File dadosAdd) throws IOException {
        StringWriter modelTsTempSb = new StringWriter();
        templateDadosAdd.merge(context, modelTsTempSb);
        System.out.println(modelTsTempSb.toString());

        try (FileWriter arq = new FileWriter(dadosAdd.toString()); BufferedWriter writer = new BufferedWriter(arq)) {
            writer.write(modelTsTempSb.toString());
            writer.flush();
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        jDialog1 = new javax.swing.JDialog();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        jCB_Sgdb = new javax.swing.JComboBox<>();
        jLabel2 = new javax.swing.JLabel();
        jTUsuario = new javax.swing.JTextField();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jPassSenha = new javax.swing.JPasswordField();
        jButton1 = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        jTBanco = new javax.swing.JTextField();
        jTServidor = new javax.swing.JTextField();
        jLabel8 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jList1 = new javax.swing.JList<>();
        jLabel6 = new javax.swing.JLabel();
        jPanel3 = new javax.swing.JPanel();
        jLabel7 = new javax.swing.JLabel();
        jScrollPane2 = new javax.swing.JScrollPane();
        jList2 = new javax.swing.JList<>();
        jPanel4 = new javax.swing.JPanel();
        jBGerar = new javax.swing.JButton();
        jTCaminhoViews = new javax.swing.JTextField();
        jBView = new javax.swing.JButton();
        jTCaminhoModelo = new javax.swing.JTextField();
        jBModelo = new javax.swing.JButton();
        jTCaminhoService = new javax.swing.JTextField();
        jBService = new javax.swing.JButton();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();

        javax.swing.GroupLayout jDialog1Layout = new javax.swing.GroupLayout(jDialog1.getContentPane());
        jDialog1.getContentPane().setLayout(jDialog1Layout);
        jDialog1Layout.setHorizontalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jDialog1Layout.setVerticalGroup(
            jDialog1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel1.setText("Generate CRUD Angular");

        jCB_Sgdb.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "MySql", "Sql Serve", "Postgress" }));

        jLabel2.setText("Selecione o SGDB");

        jLabel3.setText("Usuario do Banco");

        jLabel4.setText("Senha do Banco");

        jButton1.setText("Conectar");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel5.setText("Banco de Dados");

        jLabel8.setText("Servidor");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel2))
                    .addComponent(jCB_Sgdb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(16, 16, 16)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 105, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jTServidor, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTBanco, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jTUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPassSenha)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 109, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jButton1)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                .addGap(286, 286, 286)
                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                .addGap(265, 265, 265))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel1)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel8)
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel1Layout.createSequentialGroup()
                            .addComponent(jLabel5)
                            .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel2)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jCB_Sgdb, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel3)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                    .addComponent(jTUsuario, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jPassSenha, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jButton1)
                                    .addComponent(jTBanco, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(jTServidor, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addComponent(jLabel4))
                        .addContainerGap(19, Short.MAX_VALUE))))
        );

        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jScrollPane1.setViewportView(jList1);

        jLabel6.setText("TABELAS");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jScrollPane1)
                    .addComponent(jLabel6))
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel6)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 147, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel3.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jLabel7.setText("CAMPOS");

        jScrollPane2.setViewportView(jList2);

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                    .addComponent(jScrollPane2)
                    .addComponent(jLabel7))
                .addContainerGap())
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel3Layout.createSequentialGroup()
                .addGap(10, 10, 10)
                .addComponent(jLabel7)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane2)
                .addContainerGap())
        );

        jPanel4.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));

        jBGerar.setText("Gerar");
        jBGerar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBGerarActionPerformed(evt);
            }
        });

        jBView.setText("Caminho");
        jBView.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBViewActionPerformed(evt);
            }
        });

        jBModelo.setText("Caminho");
        jBModelo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBModeloActionPerformed(evt);
            }
        });

        jBService.setText("Caminho");
        jBService.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jBServiceActionPerformed(evt);
            }
        });

        jLabel9.setText("Caminho do Modelo");

        jLabel10.setText("Caminho do Service");

        jLabel11.setText("Caminho das View's");

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel9))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel11))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jLabel10))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jTCaminhoViews, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBView))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jTCaminhoModelo, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBModelo))
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jTCaminhoService, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jBService)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jBGerar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel4Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addComponent(jBGerar, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addGroup(jPanel4Layout.createSequentialGroup()
                        .addComponent(jLabel9)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTCaminhoModelo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jBModelo))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel10)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTCaminhoService, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jBService))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel11)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jTCaminhoViews, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jBView))))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(6, 6, 6)
                        .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel4, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed

        if (jTServidor.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe o caminho do Servidor");
            return;
        };

        if (jTBanco.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe o Banco de Dados");
            return;
        };

        if (jTUsuario.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe o Usuario");
            return;
        };

        if (jPassSenha.getText().equals("")) {
            JOptionPane.showMessageDialog(null, "Informe a Senha");
            return;
        };

        final frmCarregando loading = new frmCarregando();
        loading.setVisible(true);

        Thread t = new Thread() {
            @Override
            public void run() {
                super.run();
                conexao.setServidor(jTServidor.getText());
                conexao.setUsuario(jTUsuario.getText());
                conexao.setSenha(jPassSenha.getText());
                conexao.setBanco(jTBanco.getText());
                conexao.setUrl(conexao.getUrl().replace("#SERVIDOR", jTServidor.getText()).replace("#BANCO", jTBanco.getText()));

                try {
                    criarListTabela(conectaBanco(conexao));
                    loading.dispose();
                    JOptionPane.showMessageDialog(null,
                            "Dados para Conexão:"
                            + "\nBanco: " + conexao.getBanco()
                            + "\nDriver: " + conexao.getDriver()
                            + "\nUsuario: " + conexao.getUsuario()
                            + "\nSenha: " + conexao.getSenha()
                            + "\nUrl: " + conexao.getUrl()
                            + "\nServidor: " + conexao.getUrl());
                } catch (Exception ex) {
                    Logger.getLogger(frmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                }
            }
        };

        t.start();
    }//GEN-LAST:event_jButton1ActionPerformed

    private void jBViewActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBViewActionPerformed
        JFileChooser fc1 = new JFileChooser();
        fc1.setDialogTitle("Selecione o Diretorio...");
        fc1.resetChoosableFileFilters();
        
        /**/
        UIManager.put("FileChooser.openDialogTitleText", "Seleçao de aquivos");
        UIManager.put("FileChooser.lookInLabelText", "Local");
        UIManager.put("FileChooser.openButtonText", "Inserir");
        UIManager.put("FileChooser.cancelButtonText", "Sair");
        UIManager.put("FileChooser.fileNameLabelText", "Nome do Arquivo");
        UIManager.put("FileChooser.filesOfTypeLabelText", "Tipo de Arquivo");
        UIManager.put("FileChooser.openButtonToolTipText", "Abrir Selecionado");
        UIManager.put("FileChooser.cancelButtonToolTipText", "Sair");
        UIManager.put("FileChooser.fileNameHeaderText", "Nome do Arquivo");
        UIManager.put("FileChooser.upFolderToolTipText", "Subir Nivel Acima");
        UIManager.put("FileChooser.homeFolderToolTipText", "Desktop");
        UIManager.put("FileChooser.newFolderToolTipText", "Nova Pasta");
        UIManager.put("FileChooser.listViewButtonToolTipText", "Lista");
        UIManager.put("FileChooser.newFolderButtonText", "Criar Nova Pasta");
        UIManager.put("FileChooser.renameFileButtonText", "Renomear");
        UIManager.put("FileChooser.deleteFileButtonText", "Apagar");
        UIManager.put("FileChooser.filterLabelText", "Tipo de Arquivos");
        UIManager.put("FileChooser.detailsViewButtonToolTipText", "Detalhes");
        UIManager.put("FileChooser.fileSizeHeaderText", "Tamanho");
        UIManager.put("FileChooser.fileDateHeaderText", "Data de Modificação");

        SwingUtilities.updateComponentTreeUI(fc1);
        /**/
        
        fc1.setFileSelectionMode(JFileChooser.FILES_AND_DIRECTORIES);

        int res = fc1.showOpenDialog(this);

        File caminho = null;

        if (res == JFileChooser.APPROVE_OPTION) {
            caminho = fc1.getSelectedFile();
            jTCaminhoViews.setText(caminho.getAbsolutePath());
            jBGerar.setEnabled(true);
            //JOptionPane.showMessageDialog(null, "Pasta selecionada: " + caminho.getName());
        } else {
            JOptionPane.showMessageDialog(null, "Operação cancelada.");

        }
    }//GEN-LAST:event_jBViewActionPerformed

    private void jBGerarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBGerarActionPerformed

        final frmCarregando loading = new frmCarregando();
        loading.setVisible(true);
        Thread t = new Thread() {
            @Override
            public void run() {
                super.run();
                try {
                    Object indices[] = jList1.getSelectedValues();
                    for(int i = 0; i < indices.length; i++){
                        System.out.println("indices: " + indices[i]);
                        tabelaSelecionada = tabelaPeloNome((String) indices[i]);
                        gerarTemplates(tabelaSelecionada);
                        System.out.println("Tela Criada: " + indices[i]);
                    }
                    loading.dispose();
                } catch (IOException | URISyntaxException ex) {
                    Logger.getLogger(frmPrincipal.class.getName()).log(Level.SEVERE, null, ex);
                } 
            }
        };

        t.start();
    }//GEN-LAST:event_jBGerarActionPerformed

    private void jBModeloActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBModeloActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jBModeloActionPerformed

    private void jBServiceActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jBServiceActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jBServiceActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frmPrincipal.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new frmPrincipal().setVisible(true);
            }
        });
    }


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.JButton jBGerar;
    private javax.swing.JButton jBModelo;
    private javax.swing.JButton jBService;
    private javax.swing.JButton jBView;
    private javax.swing.JButton jButton1;
    private javax.swing.JComboBox<String> jCB_Sgdb;
    private javax.swing.JDialog jDialog1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JList<String> jList1;
    private javax.swing.JList<String> jList2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPasswordField jPassSenha;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JTextField jTBanco;
    private javax.swing.JTextField jTCaminhoModelo;
    private javax.swing.JTextField jTCaminhoService;
    private javax.swing.JTextField jTCaminhoViews;
    private javax.swing.JTextField jTServidor;
    private javax.swing.JTextField jTUsuario;
    // End of variables declaration//GEN-END:variables
}
